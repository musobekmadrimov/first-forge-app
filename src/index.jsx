import ForgeUI, {
  render,
  Fragment,
  IssuePanel,
  useState,
  useEffect,
  Form,
  TextField,
  Button,
} from "@forge/ui";
import ArrowRightCircleIcon from "@atlaskit/icon/glyph/arrow-right-circle";
import { storage } from "@forge/api";

const App = () => {
  const [inputText, setInputText] = useState("");
  useEffect(async () => {
    const textInput = await storage.get("something-interesting");
    console.log(textInput);
    setInputText(textInput);
  }, [inputText]);

  console.log("MyText is on console");

  const submitHandler = async (data) => {
    console.log(data);
    storage.set("something-interesting", data["something-interesting"]);
    setInputText(data["something-interesting"]);
  };

  return (
    <Fragment>
      <Form onSubmit={submitHandler}>
        <TextField
          name="something-interesting"
          placeholder="Write something here, save and then reload.."
          defaultValue={inputText}
        />
      </Form>
    </Fragment>
  );
};

export const run = render(
  <IssuePanel>
    <App />
  </IssuePanel>
);
